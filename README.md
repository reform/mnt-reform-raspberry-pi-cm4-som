# MNT RCM4

## 1. Working with Banana Pi CM4 (Amlogic A311D)

RCM4 is optimized to work with BPi CM4 with Amlogic A311D processor in a plug-and-play fashion. To get started, flash our system image (from https://mnt.re/system-image) to an SD card and you are ready to go. Please see the PDF manual: https://mntre.com/documentation/reform-rcm4-bpi-bundle-manual.pdf

## 2. Working with Raspberry Pi CM4

We don't yet provide a system image for using Raspberry Pi CM4 in MNT Reform, but you can adjust the official Raspberry Pi OS to make everything work.

### SD and eMMC

eMMC and SD card pins are shared on the Raspberry Pi CM4. That means you can only boot from SD card on a CM4 with no eMMC. If you want to use an eMMC-based CM4, you have to provision it first in another baseboard over USB (for example, the CM4IO or Piunora). Consult the official Raspberry Pi CM4 documentation for more details.

Advanced: if you want to use the SD card slot for data exchange but boot from eMMC, you have to transplant (resolder) RN2, R32 and R34 to the unpopulated pads of RN1, R31 and R33 on RCM4, and enable the second SD card slot in an overlay on GPIOs 22-27 (see schematic).

### Display and Audio

MNT Reform's internal display is connected via our custom HDMI to eDP adapter based on STPD2600. It is plug-and-play except for the backlight PWM.

For backlight control, deploy our `rcm4-overlay.dtbo` from this git repository to `/boot/overlays` and load it in your `/boot/config.txt` with `dtoverlay=rcm4-overlay` (see the end of this guide for all modifications to `config.txt`). This also contains the nodes for the WM8960 audio codec and RTC chip on the MNT Reform motherboard.

To enable sound, the following line must be added to `/etc/modules`:

`snd-soc-wm8960`

Even if WM8960 audio is correctly detected, you might still not get audio output. If this is the case, select the WM8960 sound card in `alsamixer` and enable the `Left Output Mixer PCM` and `Right Output Mixer PCM` toggles and make sure `Playback` is turned all the way up.

### USB Hub Reset and Backlight Power

The internal USB hub of MNT Reform needs to be taken out of reset for the input devices and lower 2 USB ports to work. You can achieve this by adding the following GPIO toggles to `/etc/rc.local`:

```
gpioset 0 10=0 # Assert USB hub reset
gpioset 0 10=1 # Deassert USB hub reset

# Enable the backlight
echo 0 > /sys/class/backlight/backlight/bl_power
```

### Recommended changes to Boot Config

The following are all recommended additions to `/boot/config.txt`:

```
# Audio prerequisites
dtparam=audio=on
dtparam=i2s=on
dtoverlay=i2c0

# GPIO16 and GPIO12 are connected for PWM compat with BPi
# switch 16 to input to avoid shorting
gpio=16=ip
# Backlight Enable
gpio=17=op,dh
# Backlight PWM, default to full on
gpio=12=op,dh

# Default internal display mode
hdmi_group:1=1
hdmi_mode:1=16

# Overclock ARM and GPU
over_voltage=6
arm_freq=2000
gpu_freq=650

# USB host mode
dtoverlay=dwc2,dr_mode=host

# Debug UART
dtoverlay=uart0,txd0_pin=14,rxd0_pin=15

# MNT Reform backlight, audio and clock
dtoverlay=rcm4-overlay
```
