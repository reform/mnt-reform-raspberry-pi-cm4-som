import r2pipe
import sys
import time
r2=r2pipe.open("gprobe://"+sys.argv[1], flags=['-n','-w'])
r2.cmd(":reset 0")
time.sleep(1)
print("reset.")
r2.cmd("wp ./isp.rapatch")
r2.cmd(":runcode 0x1800")
time.sleep(1)
print("isp running.")
r2.cmd(":flasherase 0xffff")
print("flash erased.")
r2.cmd(":flashwrite 0x200 0x400000 STDP2600_HDMI2DP_STD_RC3_3.bin")

