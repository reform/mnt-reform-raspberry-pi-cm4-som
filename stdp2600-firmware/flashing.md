# Run Radare2

```bash
radare2 -w -n gprobe:///dev/ttyUSB4
```

# R2 Commands for Flashing

```radare2
:reset 0
wp ./isp.rapatch
:runcode 0x1800
:flasherase 0xffff
:flashwrite 0x200 0x400000 STDP2600_HDMI2DP_STD_RC3_3.bin
:listen
```

